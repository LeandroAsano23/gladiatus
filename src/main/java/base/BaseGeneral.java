package base;

import constants.Constants;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import pages.*;
import pages.alliance.Alliance;
import pages.circo.BaseCirco;
import pages.circo.Turma;
import pages.topbar.Packages;
import utils.WaitUtils;
import utils.WebDriverUtils;

public class BaseGeneral extends BasePage{
    //Topbar
    @FindBy(how = How.CSS, using = "#menue_packages")
    protected WebElement packagesButton;

    @FindBy(how = How.CSS, using = "#sstat_gold_val")
    protected WebElement money;

    @FindBy(how = How.CSS, using = "div#header_values_hp_percent")
    protected WebElement healthBarPercent;

    @FindBy(how = How.CSS, using = "span#expeditionpoints_value_point")
    protected WebElement expeditionPointsValue;

    @FindBy(how = How.CSS, using = "span#expeditionpoints_value_pointmax")
    protected WebElement expeditionPointsMax;

    @FindBy(how = How.CSS, using = "span#dungeonpoints_value_point")
    protected WebElement dungeonPointsValue;

    @FindBy(how = How.CSS, using = "span#dungeonpoints_value_pointmax")
    protected WebElement dungeonPointsMax;

    @FindBy(how = How.XPATH, using = "//div[@id='cooldown_bar_text_expedition' and contains(text(),'Ir a la Expedición')]")
    protected WebElement expeditionButtonReady;

    @FindBy(how = How.CSS, using = "div#cooldown_bar_text_expedition + a")
    protected WebElement expeditionButtonLink;

    @FindBy(how = How.CSS, using = "div#cooldown_bar_text_dungeon + a")
    protected WebElement dungeonButton;

    @FindBy(how = How.CSS, using = "div#cooldown_bar_text_ct + a")
    protected WebElement circoButton;

    //Navbar
    @FindBy(how = How.XPATH, using = "//a[@title='Visión general']")
    protected WebElement generalViewButton;

    @FindBy(how = How.XPATH, using = "//a[@title='Panteón']")
    protected WebElement panteonButton;

    @FindBy(how = How.XPATH, using = "//a[@title='Alianza']")
    protected WebElement allianceButton;

    //SubBar1

    @FindBy(how = How.XPATH, using = "//div[@id='submenu1']/a[contains(text(),'Trabajo')]")
    protected WebElement workButton;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Fundición')]")
    protected WebElement foundryButton;

    @FindBy(how = How.XPATH, using = "//div[@id='submenu1']/a[contains(text(),'Edificio de subastas')]")
    protected WebElement auctionButton;

    @FindBy(how = How.XPATH, using = "//div[@id='submenuhead2']/div[@class='menutab_city']/a")
    protected WebElement subMenu1;

    @FindBy(how = How.XPATH, using = "//div[@id='submenuhead2']/div[@class='menutab_country']/a")
    protected WebElement subMenu2;


    public BaseGeneral(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
        checkMaintance();
        checkDayBonus();
        checkObjective();
    }

    public Packages goPackages() throws MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), packagesButton, 5);
        return new Packages(getDriver());
    }

    public Expeditions goExpedition() throws MaintenanceExcepcion {
        WaitUtils.clickeableElement(getDriver(), expeditionButtonReady, 30);
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), expeditionButtonLink, 5);
        return new Expeditions(getDriver());
    }

    public Dungeon goDungeon() throws MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), dungeonButton, 5);
        return new Dungeon(getDriver());
    }

    public BaseCirco goCirco() throws MaintenanceExcepcion {
        WaitUtils.clickElement(getDriver(), circoButton, 5);
        return new Turma(getDriver());
    }

    public GeneralView goGeneralView() throws MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), generalViewButton, 3);
        return new GeneralView(getDriver());
    }

    public GeneralView goPanteon() throws MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), panteonButton, 3);
        return new GeneralView(getDriver());
    }

    public Alliance goAlliance() throws MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), allianceButton, 3);
        return new Alliance(getDriver());
    }

    public Work goWork() throws InterruptedException, MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        clickElementCityMenu(getDriver(), workButton);
        return new Work(getDriver());
    }

    public Foundry goFoundry() throws InterruptedException, MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        clickElementCityMenu(getDriver(), foundryButton);
        return new Foundry(getDriver());
    }

    public AuctionHouse goAuctionHouse() throws InterruptedException, MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        clickElementCityMenu(getDriver(), auctionButton);
        return new AuctionHouse(getDriver());
    }

    public boolean isHealthBarSufficient(int dangerHealth){
        int currentHealth = getHealthBarPercent();
        return currentHealth > dangerHealth;
    }

    public void clickElementCityMenu(WebDriver driver, WebElement webElement) throws InterruptedException {
        if (!WaitUtils.isPresent(driver, webElement, 5)){
            WebDriverUtils.performMouseOver(driver, subMenu1);
        }
        WaitUtils.clickElement(driver, webElement, 3);
    }

    public void clickElementCombatsMenu(WebDriver driver, WebElement webElement) throws InterruptedException {
        if (!WaitUtils.isPresent(driver, webElement, 5)){
            WebDriverUtils.performMouseOver(driver, subMenu2);
        }
        WaitUtils.clickElement(driver, webElement, 3);
    }

    /**
     * This method checks and close all the different windows that pop ups)
     */
    public void checkPopUps() {
        checkDayBonus();
        checkCofferAndCostume();
        checkLevelUp();
        checkObjective();
    }

    private void checkMaintance() throws MaintenanceExcepcion {
        if (WaitUtils.isPresent(this.getDriver(), Constants.maintanceMessageBy, 5)){
            Maintenance maintance = new Maintenance(getDriver());

            WaitUtils.waitMinuts(2);
            if (maintance.isActualizePresent()){
                maintance.clickActualize();
                System.out.println("actualize presente");
            }

            WaitUtils.waitMinuts(1);
            throw new MaintenanceExcepcion("Mantenimiento Presente");
        }
    }

    private void checkDayBonus(){
        PageFactory.initElements(getDriver(), this);
        if (WaitUtils.isPresent(getDriver(), Constants.bonusMessageBy, 5)){
            WebElement acceptButton = getDriver().findElement(Constants.bonusMessageAcceptButtonBy);
            WaitUtils.clickElement(getDriver(), acceptButton, 3);
        }
    }

    private void checkLevelUp() {
        if (WaitUtils.isPresent(getDriver(), Constants.levelUpMessageBy,4)){
            WebElement acceptButton = getDriver().findElement(Constants.levelUpMessageAcceptButtonBy);
            WaitUtils.clickElement(getDriver(), acceptButton, 1);
        }
    }
    private void checkCofferAndCostume() {
        if (WaitUtils.isPresent(getDriver(), Constants.cofferMessageBy,4)){
            WebElement acceptButton = getDriver().findElement(Constants.cofferNoButtonBy);
            WaitUtils.clickElement(getDriver(), acceptButton, 1);
        }
    }

    private void checkObjective(){
        if (WaitUtils.isPresent(getDriver(), Constants.objectiveReachMessageBy, 6)){
            WebElement acceptButton = getDriver().findElement(Constants.objectiveReachAcceptButtonBy);
            WaitUtils.clickElement(getDriver(), acceptButton, 1);
        }
    }

    public void selectTab(String tabNumber){
        WebDriverUtils.selectInventoryTab(getDriver(), tabNumber);
    }

    public int getExpeditionPointsValue() {
        WaitUtils.clickeableElement(getDriver(), expeditionPointsValue, 5);
        return Integer.parseInt(expeditionPointsValue.getText());
    }

    public int getExpeditionPointsMax() {
        return Integer.parseInt(expeditionPointsMax.getText());
    }

    public int getDungeonPointsValue() {
        WaitUtils.clickeableElement(getDriver(), dungeonPointsValue, 5);
        return Integer.parseInt(dungeonPointsValue.getText());
    }

    public int getDungeonPointsMax() {
        return Integer.parseInt(dungeonPointsMax.getText());
    }

    public int getHealthBarPercent() {
        WaitUtils.isPresent(getDriver(), healthBarPercent, 5);
        return Integer.parseInt(healthBarPercent.getText().replace("%",""));
    }

    public double getMoney() {
        return Double.parseDouble(money.getText().replace(".",""));
    }


}
