package base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class Driver {

    private WebDriver webdriver;

    public Driver(String browser){
        switch (browser){
            case "remoteFirefox":
                try{
                    DesiredCapabilities dc = DesiredCapabilities.firefox();
                    dc.setBrowserName("firefox");
                    dc.setPlatform(Platform.WIN10);
                    webdriver = new RemoteWebDriver(new URL("http://localhost:4444/"), DesiredCapabilities.firefox());

                } catch ( MalformedURLException e){
                    e.printStackTrace();
                }
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                webdriver = new FirefoxDriver();
                break;
            case "opera":
                WebDriverManager.operadriver().setup();
                webdriver = new OperaDriver();
                break;
            default:
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
                options.setExperimentalOption("useAutomationExtension", false);
                WebDriverManager.chromedriver().setup();
                webdriver = new ChromeDriver(options);
                break;
        }
    }

    public WebDriver getWebdriver() {
        return webdriver;
    }
}
