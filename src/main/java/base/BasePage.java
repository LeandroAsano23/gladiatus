package base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import java.util.List;
import java.util.Set;

public abstract class BasePage  {

    private WebDriver driver;

    public BasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public static boolean webElementIsPresent(By locatorBy, List<WebElement> webElementList){
        for (WebElement actualWebElement : webElementList) {
            WebElement webElement = actualWebElement.findElement(locatorBy);
            if (!webElement.isEnabled() || !webElement.isDisplayed()){
                return false;
            }
        }
        return true;
    }

    public static void switchToWindowDesired(WebDriver driver,String windowTitle){
        String parent = driver.getWindowHandle();
        String currentTabTitle;
        String desiredWindow = null;
        Set<String> windowsOpened = driver.getWindowHandles();

        for (String window : windowsOpened) {
            driver.switchTo().window(window);
            if (!parent.equals(window)) {
                driver.switchTo().window(window);
                currentTabTitle = driver.getTitle().toLowerCase();
                if (currentTabTitle.contains(windowTitle)) {
                    desiredWindow = window;
                }
            }
        }
        driver.switchTo().window(desiredWindow);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void dispose(){
        if (driver != null){
            driver.quit();
        }
    }

}
