package pages;

import base.BaseGeneral;
import com.sun.media.jfxmedia.logging.Logger;
import excepcions.MaintenanceExcepcion;
import constants.Constants;
import constants.Items;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utils.Utils;
import utils.WaitUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.Level;

public class AuctionHouse extends BaseGeneral {

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Para Gladiadores')]")
    private WebElement gladiatorsTab;

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Para Mercenarios')]")
    private WebElement mercenariesTab;

    @FindBy(how = How.XPATH, using = "//span[@class='description_span_right']")
    private WebElement timeRemaining;

    @FindBy(how = How.XPATH, using = "//input[@value='Filtro']")
    private WebElement filterButton;

    @FindBy(how = How.XPATH, using = "//select[@name='itemType']")
    private WebElement itemType;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'item-i')]")
    private List<WebElement> items;

    public AuctionHouse(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public AuctionHouse clickGladiatorsTab() throws MaintenanceExcepcion {
        WaitUtils.clickElement(getDriver(), gladiatorsTab, 5);
        return new AuctionHouse(getDriver());
    }

    public AuctionHouse clickMercenariesTab() throws MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), mercenariesTab, 3);
        return new AuctionHouse(getDriver());
    }

    public void selectObjectType(String option) {
        WaitUtils.isClickeableElement(getDriver(), itemType, 5);
        new Select(getDriver().findElement(Constants.itemTypeDropdownBy)).selectByVisibleText(option);
    }

    public AuctionHouse clickFilter() throws MaintenanceExcepcion {
        WaitUtils.clickElement(getDriver(), filterButton, 5);
        return new AuctionHouse(getDriver());
    }

    /**
     *
     * @param itemMods string that contains a form "(PREFIX)-(ITEM TYPE)"
     */

    public List<String> getItems(String itemMods) {
        String[] itemSplited = itemMods.split("-");

        switch (itemSplited.length){
            case 1: return findItemByMods(itemMods, items);
            case 2:
                String mod1 = itemSplited[0];
                String mod2 = itemSplited[1];
                return (isSearchByItemType(itemSplited)) ? getElementsByItemTypeAndOneMod(itemSplited[1]) : findItemByMods(mod1, mod2, items);
            case 3: return getElementsByItemTypeAndMods(itemSplited);
            default:
                throw new NoSuchElementException("Invalid Item Info Entered");

        }
    }

    /**
     * @param itemSplited
     * @return boolean is the object mods parameters contains an item type like "ARMADURA"/"ARMA"
     */

    private boolean isSearchByItemType(String[] itemSplited) {
        for (Items enumItem: Items.values()) {
            if (enumItem.getTypeName().equalsIgnoreCase(itemSplited[1])) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param itemType
     * @retur list of string that contains elements of pattern "Lucius-Arma"/"Delicadeza-Armadura"
     */

    private List<String> getElementsByItemTypeAndOneMod(String itemType) {
        List<WebElement> filteredElements = null;

        for (Items enumItem: Items.values()) {
            if (enumItem.getTypeName().equalsIgnoreCase(itemType)){
                filteredElements = getDriver().findElements(By.xpath(enumItem.getXpath()));
            }
        }

        return findItemByMods(itemType, Objects.requireNonNull(filteredElements));
    }

    /**
     *
     * @param itemInfo
     * @return list of string that contains elements with mods of pattern "Lucius-Anillo-Delicadeza"/"Ichorus-Arma-Asesinato"
     */

    private List<String> getElementsByItemTypeAndMods(String[] itemInfo) {
        List<WebElement> filteredElements = null;
        String prefix = itemInfo[0];
        String givenType = itemInfo[1];
        String suffix = itemInfo[2];

        for (Items enumItem: Items.values()) {
            if (enumItem.getTypeName().equalsIgnoreCase(givenType)){
                filteredElements = getDriver().findElements(By.xpath(enumItem.getXpath()));
            }
        }

        return findItemByMods(prefix, suffix, Objects.requireNonNull(filteredElements));
    }

    /**
     *
     * @param itemMod
     * @param elements
     * @return list of elements that contains items with one mod like "Lucius"/"Asesinato"
     */

    private List<String> findItemByMods(String itemMod, List<WebElement> elements) {
        List<String> matchItems = new LinkedList<>();

        addMatchItemsToList(itemMod, elements, matchItems);

        return matchItems;
    }

    private void addMatchItemsToList(String itemMod, List<WebElement> elements, List<String> matchItems) {
        ForkJoinPool pool = new ForkJoinPool(20);

        try {
            pool.submit(() ->
                elements.forEach(item -> {
                    String type = Items.getTypeName(item.getAttribute("class"));
                    String price = item.getAttribute("data-price-gold");
                    String itemData = Utils.removeAccents(Utils.replaceUnicodedString(item.getAttribute("data-tooltip"))
                            .split("Refinamiento")[0]
                            .concat("Price: $"+price+".")
                            .concat("Tipo: "+type+"."));
                    String itemName = StringUtils.substringBetween(itemData, "[[[\"","\",");
                    String mod = Utils.removeAccents(itemMod);

                    if (itemName.toLowerCase().contains(mod.toLowerCase())
                            && (!itemName.contains("Poción"))
                            && (!itemName.contains("Polvos"))
                            && (Utils.arrayStrContainsValue(itemData, Constants.FOOD_TYPES))) {
                        matchItems.add(itemData);
                    }
                })).get();
        } catch (InterruptedException | ExecutionException e) {
            Logger.logMsg(Level.WARNING.intValue(), Level.WARNING.getName());
            Thread.currentThread().interrupt();
        }
    }

    /**
     *
     * @param mod1
     * @param mod2
     * @param elements
     * @return list of elements that contains entered mods like "Lucius-Asesinato"/"Ichorus-Soledad"
     */

    private List<String> findItemByMods(String mod1, String mod2, List<WebElement> elements) {
        List<String> matchItems = new LinkedList<>();

        for (WebElement item: elements) {
            String type = Items.getTypeName(item.getAttribute("class"));
            String price = item.getAttribute("data-price-gold");
            String itemData = Utils.removeAccents(Utils.replaceUnicodedString(item.getAttribute("data-tooltip"))
                    .split("Refinamiento")[0]
                    .concat("Price: $"+price+".")
                    .concat("Tipo: "+type+"."));
            String itemName = StringUtils.substringBetween(itemData, "[[[\"","\",");
            mod1 = Utils.removeAccents(mod1);
            mod2 = Utils.removeAccents(mod2);

            if (itemName.toLowerCase().contains(mod1.toLowerCase())
                    && (itemName.toLowerCase().contains(mod2.toLowerCase())
                    && (!itemName.contains("Poción"))
                    && (!itemName.contains("Polvos"))
                    && (Utils.arrayStrContainsValue(itemData, Constants.FOOD_TYPES)))){
                matchItems.add(itemData);
            }
        }

        return matchItems;
    }

    public String getTimeRemaining() {
        return timeRemaining.getText();
    }


}
