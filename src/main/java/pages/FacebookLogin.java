package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.WaitUtils;
import utils.WebDriverUtils;

public class FacebookLogin extends BasePage {

    @FindBy(how = How.CSS, using = "input#email")
    private WebElement inputEmail;

    @FindBy(how = How.CSS, using = "input#pass")
    private WebElement inputPassword;

    @FindBy(how = How.CSS, using = "label#loginbutton > input")
    private WebElement loginButton;

    public FacebookLogin(WebDriver driver) {
        super(driver);
    }

    public void loadEmail(String email){
        WaitUtils.isPresent(getDriver(), inputEmail, 3);
        inputEmail.sendKeys(email);
    }

    public void loadPassword(String password){
        WaitUtils.isPresent(getDriver(), inputPassword, 3);
        inputPassword.sendKeys(password);
    }

    public Lobby clickLogin() throws Exception {
        WaitUtils.clickElement(getDriver(), loginButton, 2);
        WebDriverUtils.switchToSecondWindow(getDriver());
        return new Lobby(getDriver());
    }

}
