package pages.circo;

import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.Utils;
import utils.WaitUtils;

import java.util.List;

public class Provinciarum extends BaseCirco {

    @FindBy(how = How.XPATH, using = "//section[@id='own3']//div[@class='attack']")
    List<WebElement> gladiators;

    public Provinciarum(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public ProvinciarumResult attackRandom() throws MaintenanceExcepcion {
        int gladiator = Utils.getRandomNumber(1,5);

        WaitUtils.clickElement(getDriver(), gladiators.get(gladiator-1), 3);
        return new ProvinciarumResult(getDriver());
    }
}
