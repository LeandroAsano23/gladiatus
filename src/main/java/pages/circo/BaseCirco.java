package pages.circo;

import base.BaseGeneral;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.WaitUtils;

public class BaseCirco extends BaseGeneral {

    @FindBy(how = How.XPATH, using = "//a[contains(@class,'awesome-tabs') and text()='Circo Turma']")
    protected WebElement turmaTab;

    @FindBy(how = How.XPATH, using = "//a[contains(@class,'awesome-tabs') and text()='Circo Provinciarum']")
    protected WebElement provinciarumTab;

    public BaseCirco(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public Provinciarum goTurma() throws MaintenanceExcepcion {
        WaitUtils.clickeableElement(getDriver(), turmaTab, 3);

        if (!turmaTab.getAttribute("class").contains("current")){
            WaitUtils.clickElement(getDriver(), turmaTab, 2);
        }
        return new Provinciarum(getDriver());
    }

    public Provinciarum goProvinciarum() throws MaintenanceExcepcion {
        WaitUtils.clickeableElement(getDriver(), provinciarumTab, 5);

        if (!provinciarumTab.getAttribute("class").contains("current")){
            WaitUtils.clickElement(getDriver(), provinciarumTab, 2);
        }

        return new Provinciarum(getDriver());

    }
}
