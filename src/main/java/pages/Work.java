package pages;

import base.BaseGeneral;
import constants.Constants;
import constants.Hours;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utils.WaitUtils;

import java.util.List;
import java.util.Optional;

public class Work extends BaseGeneral {

    @FindBy(how = How.XPATH, using = "//div[@id='select']//tr[contains(@id,'job_row')]/th")
    private List<WebElement> jobs;

    @FindBy(how = How.XPATH, using = "//select[@id='workTime']")
    private WebElement time;

    @FindBy(how = How.XPATH, using = "//input[@id='doWork']")
    private WebElement workButton;

    public Work(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public void selectJob(String jobName){
        WaitUtils.visibilityOfAllElements(getDriver(), jobs, 5);
        Optional<WebElement> workOpt = jobs.stream()
                .filter(job -> job.getText().toLowerCase().contains(jobName.toLowerCase()))
                .findFirst();

        workOpt.ifPresent(work -> WaitUtils.clickElement(getDriver(), work, 4));
    }

    public int selectTime(String timeWanted){
        WaitUtils.clickeableElement(getDriver(), time, 4);
        Select timeSelect = new Select(getDriver().findElement(Constants.timeDropdownBy));
        if (timeWanted.equals("1")){
            timeSelect.selectByVisibleText(timeWanted+" Hora");
        } else {
            timeSelect.selectByVisibleText(timeWanted+" Horas");
        }
        return Hours.getMinsByHour(timeWanted);
    }

    public void startWork(){
        WaitUtils.clickElement(getDriver(), workButton, 4);
    }


}
