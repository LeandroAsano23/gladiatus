package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WebDriverUtils;

import static utils.WaitUtils.clickElement;
import static utils.WaitUtils.sendKeys;

public class Login extends BasePage {

    @FindBy(xpath = "//span[contains(text(),'Iniciar sesión')]")
    private WebElement iniciarTab;

    @FindBy(css = "input[type=email]")
    private WebElement emailInput;

    @FindBy(css = "input[type=password]")
    private WebElement passwordInput;

    @FindBy(css = "button[class*=\"button-primary\"]")
    private WebElement loginButton;

    @FindBy(css = "button[class *= button-facebook]")
    private WebElement loginWithFacebook;

    public Login(WebDriver driver) {
        super(driver);
        driver.get("https://lobby.gladiatus.gameforge.com/es_AR/hub");
        driver.manage().window().maximize();
    }

    public void loadEmail(String email){
        sendKeys(getDriver(), emailInput, email, 2);
    }

    public void loadPassword(String password){
        sendKeys(getDriver(), passwordInput, password, 2);
    }

    public void clickIniciarTab(){
        clickElement(getDriver(), iniciarTab, 5);
    }

    public Lobby clickLogin(){
        clickElement(getDriver(), loginButton);
        return new Lobby(getDriver());
    }

    public FacebookLogin clickLoginThroughFacebook() throws Exception {
        clickElement(getDriver(), loginWithFacebook);
        WebDriverUtils.switchToSecondWindow(getDriver());
        return new FacebookLogin(getDriver());
    }


}
