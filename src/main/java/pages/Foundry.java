package pages;

import base.BaseGeneral;
import excepcions.MaintenanceExcepcion;
import com.slack.api.methods.SlackApiException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.SlackUtils;
import utils.WaitUtils;
import utils.WebDriverUtils;

import java.io.IOException;
import java.util.List;

public class Foundry extends BaseGeneral {

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'forge_finished-succeeded')]")
    private List<WebElement> readyFoundries;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'forge_closed')]")
    private List<WebElement> freeFoundries;

    @FindBy(how = How.XPATH, using = "//div[@id='inv']//div[contains(@class,'item-i-')]")
    private List<WebElement> inventoryItems;

    @FindBy(how = How.XPATH, using = "//div[@id='forge_lootbox']")
    private WebElement sendMaterials;

    @FindBy(how = How.XPATH, using = "//div[@id='itembox']")
    private WebElement meltArea;

    @FindBy(how = How.XPATH, using = "//fieldset[@id='rent']/div")
    private WebElement meltButton;

    @FindBy(how = How.XPATH, using = "//fieldset[@id='rent']/div/span")
    private WebElement meltPriceSpan;

    public Foundry(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public void getMeltedItems(BaseGeneral page) throws IOException, SlackApiException, MaintenanceExcepcion {

        if (readyFoundries.stream().findFirst().isPresent()){
            WaitUtils.visibilityOfAllElements(getDriver(), readyFoundries, 5);

            int itemsPicked = 0;
            int totalsize = readyFoundries.size();

            for (int i = 0; i < totalsize; i++) {
                WaitUtils.waitSeconds(4);

                if (readyFoundries.stream().findFirst().isPresent()){
                    WebElement readyFoundry = readyFoundries.stream().findFirst().get();
                    WaitUtils.clickElement(page.getDriver(), readyFoundry, 4);
                    WaitUtils.clickElement(page.getDriver(), sendMaterials, 4);
                    page = new Foundry(page.getDriver());
                    itemsPicked++;
                }
            }

            SlackUtils.sendMessage(":heavy_check_mark: Recogiste "+itemsPicked+" Items Fundidos");
        } else {
            SlackUtils.sendMessage(":clock1: Los Items siguen en proceso");
        }
    }

    public void meltItem(BaseGeneral page, String tabNumber) throws InterruptedException, IOException, SlackApiException, MaintenanceExcepcion {
        WebDriverUtils.selectInventoryTab(getDriver(), tabNumber);

        if (freeFoundries.stream().findFirst().isPresent()){
            WaitUtils.visibilityOfAllElements(getDriver(), freeFoundries, 5);

            int newMeltingItems = 0;
            int totalSize = freeFoundries.size();
            int totalItems = inventoryItems.size();

            for (int i = 0; i < totalSize; i++){
                WaitUtils.waitSeconds(4);

                if (freeFoundries.stream().findFirst().isPresent()){
                    WebElement freeFoundry = freeFoundries.stream().findFirst().get();
                    WaitUtils.clickElement(page.getDriver(), freeFoundry, 4);

                    if (inventoryItems.stream().findFirst().isPresent()){
                        WebElement itemToMelt = inventoryItems.stream().findFirst().get();
                        WebDriverUtils.performDragAndDrop(page.getDriver(), itemToMelt, meltArea);

                        WaitUtils.clickeableElement(page.getDriver(), meltPriceSpan, 5);
                        WaitUtils.clickeableElement(page.getDriver(), this.money, 5);
                        double meltPrice = Double.parseDouble(meltPriceSpan.getText());

                        if (this.getMoney()>=meltPrice){
                            WaitUtils.clickElement(getDriver(), meltButton, 4);
                            page = new Foundry(page.getDriver());

                            newMeltingItems++;
                        } else {
                            SlackUtils.sendMessage(":warning: No tienes dinero suficiente para fundir items");
                            break;
                        }
                    } else {
                        SlackUtils.sendMessage(":warning: No tienes Items para Fundir en la tab "+tabNumber);
                        break;
                    }
                }
            }

            SlackUtils.sendMessage(":fire: Pusiste "+newMeltingItems+" a Fundir - Items restantes: "+(totalItems-newMeltingItems));
        }
    }

}
