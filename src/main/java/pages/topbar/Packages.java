package pages.topbar;

import base.BaseGeneral;
import excepcions.MaintenanceExcepcion;
import com.slack.api.methods.SlackApiException;
import constants.ItemsMelteable;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.SlackUtils;
import utils.WaitUtils;
import utils.WebDriverUtils;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class Packages extends BaseGeneral {

    @FindBy(how = How.XPATH, using = "//div[@id='packages']//div[@class='packageItem']/div/div")
    private List<WebElement> packages;

    @FindBy(how = How.XPATH, using = "//div[@id='inventory_nav']//a[contains(@class,'awesome-tabs')]")
    private List<WebElement> inventoryTabs;

    @FindBy(how = How.XPATH, using = "//div[@id='inv']")
    private WebElement inventory;

    @FindBy(how = How.XPATH, using = "//div[@class='ui-droppable grid-droparea']")
    private WebElement dropeableSquare;

    @FindBy(how = How.XPATH, using = "//h2[contains(text(),'Contenido')]")
    private WebElement contentSpan;

    @FindBy(how = How.XPATH, using = "(//a[contains(@class,'paging_left_step')])[1]")
    private WebElement previousPage;

    @FindBy(how = How.XPATH, using = "(//a[contains(@class,'paging_right_full')])[1]")
    private WebElement lastPage;

    public Packages(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public void dragItemToInventory() throws InterruptedException {
        WebElement itemToDrag = getFirstItem();
        WaitUtils.clickeableElement(getDriver(), itemToDrag, 4);
        WebDriverUtils.performClickAndHoldWithIntermediateElemDrop(getDriver(), itemToDrag, contentSpan, dropeableSquare);
    }

    private WebElement getFirstItem(){
        WaitUtils.visibilityOfAllElements(getDriver(), packages, 5);
        WebElement item =  packages.stream().findFirst().get();
        return item;
    }

    public void fullInventory(String tab) throws MaintenanceExcepcion, InterruptedException, IOException, SlackApiException {
        WaitUtils.clickElement(getDriver(), lastPage, 5);
        boolean slotsAvailable = true;
        int contItems = 0;

        while (slotsAvailable){
            Optional<WebElement> itemToDrag = getMelteableItem(this);
            WebDriverUtils.selectInventoryTab(getDriver(), tab);

            WebDriverUtils.performDragAndDrop(this.getDriver(), itemToDrag, contentSpan);
            WaitUtils.waitSeconds(4);

            if (WaitUtils.isPresent(getDriver(), dropeableSquare, 3)){
                WebDriverUtils.performDragAndDrop(getDriver(), itemToDrag, dropeableSquare);
                WaitUtils.waitSeconds(4);
                contItems++;
            } else {
                slotsAvailable = false;
            }
        }

        String message = (contItems==0)
                ? ":chest1: Inventario full"
                : ":chest1: "+contItems+" Items Añadidos al Inventario";
        SlackUtils.sendMessage(message);
    }

    private Optional<WebElement> getMelteableItem(BaseGeneral page) throws MaintenanceExcepcion {
        WaitUtils.visibilityOfAllElements(getDriver(), packages, 5);

        boolean isItemMeleteable = packages.stream().noneMatch(pack -> ItemsMelteable.contains(pack.getAttribute("class")));
        while (isItemMeleteable){
            WaitUtils.clickElement(page.getDriver(), previousPage, 5);
            page = new BaseGeneral(page.getDriver());
            isItemMeleteable = packages.stream().noneMatch(pack -> ItemsMelteable.contains(pack.getAttribute("class")));
        }

        return packages.stream()
                .filter(pack -> ItemsMelteable.contains(pack.getAttribute("class")))
                .findFirst();
    }


}
