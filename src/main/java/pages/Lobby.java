package pages;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WaitUtils;
import utils.WebDriverUtils;

public class Lobby extends BasePage {

    @FindBy(css = ".button-default")
    private WebElement jugadoButton;

    public Lobby(WebDriver driver) {
        super(driver);
    }

    public GeneralView clickJugado() throws Exception {
        WaitUtils.waitSeconds(4);
        WaitUtils.clickeableElement(getDriver(), jugadoButton, 5);
        jugadoButton.click();
        WebDriverUtils.switchToSecondWindow(getDriver(), "Provincia");
        return new GeneralView(getDriver());
    }
}
