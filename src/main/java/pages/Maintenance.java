package pages;

import base.BaseGeneral;
import base.BasePage;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.WaitUtils;

public class Maintenance extends BasePage {

    @FindBy(how = How.CSS, using = ".button-default")
    private WebElement actualize;

    public Maintenance(WebDriver driver) {
        super(driver);
    }

    public boolean isActualizePresent(){
        return WaitUtils.isPresent(getDriver(), actualize, 4);
    }

    public BaseGeneral clickActualize() throws MaintenanceExcepcion {
        WaitUtils.clickElement(getDriver(), actualize, 4);
        return new BaseGeneral(getDriver());
    }
}
