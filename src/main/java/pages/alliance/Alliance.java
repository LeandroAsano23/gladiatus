package pages.alliance;

import base.BaseGeneral;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.WaitUtils;

public class Alliance extends BaseGeneral {

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Mercado de la Alianza')]")
    private WebElement market;

    public Alliance(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public AllianceMarket goMarket() throws InterruptedException, MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickElement(getDriver(), market, 3);
        return new AllianceMarket(getDriver());
    }

}
