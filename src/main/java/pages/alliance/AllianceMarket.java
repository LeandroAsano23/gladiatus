package pages.alliance;

import base.BaseGeneral;
import excepcions.MaintenanceExcepcion;
import com.slack.api.methods.SlackApiException;
import constants.Constants;
import constants.Rotatives;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utils.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AllianceMarket extends BaseGeneral {

    private static final String NAME = "name";

    private static final String VENDOR = "vendor";

    private static final String BUY_BUTTON = "buyButton";

    private static final String VALUE = "value";

    @FindBy(how = How.XPATH, using = "//div[@id='market_sell']/div")
    private WebElement sellArea;

    @FindBy(how = How.XPATH, using = "//input[@name='preis']")
    private WebElement price;

    @FindBy(how = How.XPATH, using = "//select[@id='dauer']")
    private WebElement duration;

    @FindBy(how = How.XPATH, using = "//input[@value='Ofertar']")
    private WebElement offerButton;

    @FindBy(how = How.XPATH, using = "//table[@id='market_item_table']//following-sibling::tr[1]")
    private List<WebElement> saleItems;

    public AllianceMarket(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    /**
     *
     * @return Web Element bought
     * @throws IOException
     * @throws SlackApiException
     */

    public String buyItem() throws IOException, SlackApiException {
        WebElement maxPurchasableItem = findFirstBuyableItem();

        if (maxPurchasableItem != null){
            maxPurchasableItem = findMaxBuyableItem(maxPurchasableItem);
            HashMap<String, Object> itemInfo = getSaleItemInfo(maxPurchasableItem);

            String itemName = itemInfo.get(NAME).toString();
            String vendor = itemInfo.get(VENDOR).toString();
            WebElement buyButton = (WebElement) itemInfo.get(BUY_BUTTON);

            WaitUtils.clickElement(getDriver(), buyButton, 4);
            SlackUtils.sendMessage(":dollar: Compraste \""+itemName+"\" a \""+vendor+"\"");
            return itemName;
        }
        return null;
    }

    /**
     *
     * @param itemToSellName
     * @throws InterruptedException
     */

    public void sellItem(String itemToSellName) throws InterruptedException, IOException, SlackApiException {
        By itemToSellBy = By.xpath(String.format(Constants.INVENTORY_ITEM_BOUGHT_TEMPLATE, Utils.replaceStringToUnicode(itemToSellName)));
        WebElement itemToSell = getDriver().findElement(itemToSellBy);
        String priceStr = Rotatives.getPriceByName(itemToSellName);

        setDuration();
        WebDriverUtils.performDragAndDrop(getDriver(), itemToSell, sellArea);
        WaitUtils.clearAndsendKeys(getDriver(), price, priceStr, 3);
        WaitUtils.clickElement(getDriver(), offerButton, 3);

        SlackUtils.sendMessage(":moneybag: Vendiste "+itemToSellName+" a $"+priceStr);
    }

    /**
     * @return first Web Element buyable of any price
     */

    private WebElement findFirstBuyableItem() {

        for (WebElement saleItem : saleItems) {
            HashMap<String, Object> itemInfo = getSaleItemInfo(saleItem);

            WebElement buyButton = (WebElement) itemInfo.get(BUY_BUTTON);

            if (!buyButton.getAttribute("class").contains("disabled") && !buyButton.getAttribute(VALUE).contains("Cancelar")) {
                double itemValue = Double.parseDouble(itemInfo.get(VALUE).toString());
                double itemCommission = itemValue*0.04;

                if (this.getMoney() >= (itemValue+itemCommission)) {
                    return saleItem;
                }

            }
        }

        return null;
    }

    /**
     * @param maxPurchasableItem
     * @return the most expensive Web Element that the user can pay for it
     */

    private WebElement findMaxBuyableItem(WebElement maxPurchasableItem) {
        for (WebElement item : saleItems) {
            HashMap<String, Object> itemInfo = getSaleItemInfo(item);
            WebElement buyButton = (WebElement) itemInfo.get(BUY_BUTTON);

            if (!buyButton.getAttribute("class").contains("disabled") && !buyButton.getAttribute(VALUE).contains("Cancelar")) {

                HashMap<String, Object> maxBuyInfo = getSaleItemInfo(maxPurchasableItem);

                double maxPurchasableItemValue = Double.parseDouble(maxBuyInfo.get(VALUE).toString());

                double itemValue = Double.parseDouble(itemInfo.get(VALUE).toString());
                double itemCommission = itemValue*0.04;


                if (this.getMoney() >= (itemValue+itemCommission) && (maxPurchasableItemValue<itemValue)) {
                    maxPurchasableItem = item;
                }
            }
        }
        return maxPurchasableItem;
    }

    private HashMap<String, Object> getSaleItemInfo(WebElement item) {
        HashMap<String,Object> saleItemInfo = new LinkedHashMap<>();

        Map<String,String> itemInfo = ItemUtils.getCharacteristicsWithAccents(item.findElement(By.xpath("td[1]/div")));
        saleItemInfo.put(NAME, itemInfo.get("Nombre"));
        saleItemInfo.put(VENDOR, item.findElement(By.xpath("td[2]")).getText());
        saleItemInfo.put(VALUE, item.findElement(By.xpath("td[3]")).getText().replace(".",""));
        saleItemInfo.put(BUY_BUTTON, item.findElement(By.xpath("td[6]/input")));

        return saleItemInfo;
    }

    private void setDuration(){
        WaitUtils.clickeableElement(getDriver(), duration, 5);
        Select durationSelect = new Select(getDriver().findElement(Constants.durationDropdownBy));
        durationSelect.selectByVisibleText(Constants.DURATION_24HS);
    }
}
