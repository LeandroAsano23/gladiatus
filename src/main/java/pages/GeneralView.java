package pages;

import base.BaseGeneral;
import com.slack.api.methods.SlackApiException;
import constants.Constants;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.SlackUtils;
import utils.WaitUtils;
import utils.WebDriverUtils;

import java.io.IOException;
import java.util.Arrays;

public class GeneralView extends BaseGeneral {

    @FindBy(how = How.CSS, using = "div#avatar > div.ui-droppable")
    private WebElement avatarImage;

    public GeneralView(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public void eatSomething() throws IOException, SlackApiException {

        boolean foodPresent = Arrays.stream(Constants.FOOD_TYPES).anyMatch(foodStr -> {
            By foodBy = By.xpath(String.format(Constants.FOOD_XPATH_TEMPLATE, foodStr));
            if (WaitUtils.isPresent(getDriver(), foodBy, 2)){
                WebElement food = getDriver().findElement(foodBy);
                WaitUtils.clickeableElement(getDriver(), food, 5);

                WaitUtils.runRandomSleepBetween2And4();
                WebDriverUtils.performDragAndDrop(getDriver(),food,avatarImage);
                WaitUtils.runRandomSleepBetween2And4();

                return true;
            } else {
                return false;
            }
        });

        if (!foodPresent){
            SlackUtils.sendMessage(":hamburger: ¡¡¡FOOD EMPTY!!!! :meat_on_bone:");
            throw new NoSuchElementException("FOOD IS EMPTY!!!");
        }

    }



}
