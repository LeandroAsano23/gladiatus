package pages;

import base.BaseGeneral;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.WaitUtils;

import java.util.List;

public class Expeditions extends BaseGeneral {

    @FindBy(how = How.CSS, using = ".expedition_box")
    private List<WebElement> monsters;

    @FindBy(how = How.XPATH, using = "//a/img[@title='Rubíes']")
    private WebElement rubies;

    By attackButtonBy = By.cssSelector("button.expedition_button");

    public Expeditions(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    private WebElement searchMonster(String monsterName) {
        for (WebElement monster: monsters) {
            WaitUtils.isPresent(getDriver(), monster, 5);
            if (monster.getText().contains(monsterName)){
                return monster.findElement(attackButtonBy);
            }
        }
        return null;
    }

    /* A IMPLEMENTAR
        return monsters
                .stream()
                .filter(monster -> monster.findElement(monsterXPath).getText().contains(monsterName))
                .findFirst().get();
     */

    public ExpeditionResult attackMonster(String monsterName) throws InterruptedException, MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        if (!WaitUtils.isPresent(getDriver(), rubies, 5)){
            WebElement monster = searchMonster(monsterName);
            WaitUtils.clickElement(getDriver(), monster, 2);
        }
        return new ExpeditionResult(getDriver());
    }
}
