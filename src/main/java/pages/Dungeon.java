package pages;

import base.BaseGeneral;
import excepcions.MaintenanceExcepcion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import utils.WaitUtils;

import java.util.List;

public class Dungeon extends BaseGeneral {

    @FindBy(how = How.XPATH, using = "//h3[contains(text(),'Entrá al calabozo')]")
    private WebElement enterDungenMessage;

    @FindBy(how = How.XPATH, using = "//input[@value='Normal']")
    private WebElement enterNormalDungeonButton;

    @FindBy(how = How.XPATH, using = "//input[@value='Avanzado']")
    private WebElement enterAdvancedDungeonButton;

    @FindBy(how = How.XPATH, using = "//img[@onClick]")
    private List<WebElement> dungeons;

    public Dungeon(WebDriver driver) throws MaintenanceExcepcion {
        super(driver);
    }

    public Dungeon enterDungeon(boolean advanced) throws MaintenanceExcepcion {

        if (WaitUtils.isPresent(getDriver(), enterDungenMessage, 3)){
            if (advanced){
                if (!enterAdvancedDungeonButton.getCssValue("class").contains("disabled")){
                    WaitUtils.clickElement(getDriver(), enterAdvancedDungeonButton, 2);
                } else {
                    return null;
                }
            } else {
                WaitUtils.clickElement(getDriver(), enterNormalDungeonButton, 2);
            }
        }

        return new Dungeon(getDriver());
    }

    public DungeonResult attackDungeon() throws MaintenanceExcepcion {
        WaitUtils.runRandomSleepBetween2And4();
        WebElement monster = dungeons.stream().findFirst().get();
        WaitUtils.clickElement(getDriver(), monster, 5);
        return new DungeonResult(getDriver());
    }

}
