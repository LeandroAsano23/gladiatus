package constants;

import java.util.Optional;
import java.util.stream.Stream;

public enum Colours {
    GREEN_COLOUR("lime", ":green-icon:"),
    BLUE_COLOUR("#5159F7", ":blue-icon:"),
    PURPLE_COLOUR("#E303E0", ":purple-icon:");

    private final String colour;

    private final String icon;

    Colours(String colour, String icon) {
        this.colour = colour;
        this.icon = icon;
    }
    public String getColour() {
        return colour;
    }

    public String getIcon() {
        return icon;
    }

    public static String getColour(String item) {
        Optional<Colours> colour = Stream.of(Colours.values())
                .filter(color -> item.contains(color.getColour()))
                .findFirst();

        return colour.map(Colours::getIcon).orElse(null);
    }
}
