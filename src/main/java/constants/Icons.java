package constants;

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;
import java.util.stream.Stream;

public enum Icons {
    ARMA(":sword:"),
    ESCUDO(":shield:"),
    ARMADURA(":armor:"),
    CASCO(":head:"),
    GUANTES(":gauntelet:"),
    ANILLO(":ring:"),
    BOTAS(":boots:"),
    AMULETO(":amulet:"),
    MERCENARIO(":scroll:");


    private final String icon;

    Icons(String icon){
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public static String getIcon(String item) {
        final String tipo = StringUtils.substringBetween(item,"Tipo: ",".");

        Optional<Icons> iconOpt = Stream.of(Icons.values())
                .filter(icon -> tipo.equalsIgnoreCase(icon.toString()))
                .findFirst();

        return iconOpt.map(Icons::getIcon).orElse(null);
    }
}
