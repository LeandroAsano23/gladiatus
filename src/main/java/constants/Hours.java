package constants;

import org.apache.commons.lang3.StringUtils;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

public enum Hours {

    ONE_HOUR("1", 12),
    TWO_HOURS("2", 24),
    THREE_HOURS("3",36),
    FOUR_HOURS("4", 48),
    FIVE_HOURS("5", 60),
    SIX_HOURS("6", 72),
    SEVEN_HOURS("7", 84),
    EIGHT_HOURS("8", 96),
    NINE_HOURS("9", 108),
    TEN_HOURS("10", 120),
    ELEVEN_HOURS("11", 132),
    TWELVE_HOURSE("12", 144);

    private final String hourTime;

    private final int minsTime;

    Hours(String hour, int mins){
        this.hourTime = hour;
        this.minsTime = mins;
    }

    public static int getMinsByHour(String hourStr){
        final String hourStrFinal = StringUtils.substringBefore(hourStr, " ");

        Optional<Hours> hoursOpt =  Stream.of(Hours.values())
                .filter(hour -> hourStrFinal.equalsIgnoreCase(hour.hourTime))
                .findFirst();

        return hoursOpt.map(Hours::getMinsTime).orElseThrow(NoSuchElementException::new);
    }

    public String getHourTime() {
        return hourTime;
    }

    public int getMinsTime() {
        return minsTime;
    }

}
