package constants;

public enum ItemsMelteable {
    WEAPON("item-i-1-", "Arma", "1"),
    SHIELD("item-i-2-", "Escudo", "2"),
    ARMOR("item-i-3-", "Armadura", "3"),
    HELM("item-i-4-", "Casco", "4"),
    GLOVES("item-i-5-", "Guantes", "5"),
    BOOTS("item-i-8-", "Botas", "8");

    private final String xpath;
    private final String typeName;
    private final String typeValue;

    ItemsMelteable(String xpath, String typeName, String typeValue){
        this.xpath = xpath;
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public String getItemClass() {
        return xpath;
    }

    public String getItemName() {
        return typeName;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public static boolean contains(String itemClass){
        for (ItemsMelteable enumItem: ItemsMelteable.values()) {
            if (itemClass.contains(enumItem.getItemClass())){
                return true;
            }
        }
        return false;
    }

    public static String getItemName(String itemClass) {
        for (ItemsMelteable enumItem: ItemsMelteable.values()) {
            if (itemClass.contains(String.format(Constants.ITEMS_TYPE_XPATH_TEMPLATE, enumItem.getTypeValue()))){
                return enumItem.getItemName();
            }
        }
        return null;
    }
}

