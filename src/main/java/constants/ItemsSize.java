package constants;

enum ItemsSize {

    ONE_SLOT("1"),
    TWO_SLOTS("2"),
    THREE_SLOTS("8"),
    FOUR_SLOTS("16"),
    SIX_SLOTS("32");

    private final String size;

    ItemsSize(String size){
        this.size = size;
    }

    public static String getSlotsBySize(String size){
        for (ItemsSize itemSize: ItemsSize.values()) {
            if (itemSize.size.equals(size)){
                return itemSize.toString();
            }
        }
        return null;
    }

    public String getSize() {
        return size;
    }
}
