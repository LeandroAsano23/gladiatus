package constants;

public enum Rotatives {

    PENDIENTE_DE_MALAQUITA("Pendiente de malaquita", "2000000"),
    ANILLO_DEL_DRAGON("Anillo del dragón", "1000000"),
    ANILLO_AZULL("Anillo azul", "500000"),
    FIBULA("Fíbula", "200000"),
    OJO_DE_RA("Ojo de Ra", "100000");

    private final String nombre;

    private final String precio;

    Rotatives(String nombre, String precio){
        this.nombre = nombre;
        this.precio = precio;
    }

    public static String getPriceByName(String name){
        for (Rotatives rotative: Rotatives.values()) {
            if (rotative.nombre.contains(name)){
                return rotative.getPrecio();
            }
        }
        return null;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPrecio() {
        return precio;
    }
}
