package constants;

import java.util.Optional;
import java.util.stream.Stream;

public enum Items {
    WEAPON("//div[contains(@class,'item-i-1-')]", "Arma", "1"),
    SHIELD("//div[contains(@class,'item-i-2-')]", "Escudo", "2"),
    ARMOR("//div[contains(@class,'item-i-3-')]", "Armadura", "3"),
    HELM("//div[contains(@class,'item-i-4-')]", "Casco", "4"),
    GLOVES("//div[contains(@class,'item-i-5-')]", "Guantes", "5"),
    RING("//div[contains(@class,'item-i-6-')]", "Anillo", "6"),
    BOOTS("//div[contains(@class,'item-i-8-')]", "Botas", "8"),
    AMULET("//div[contains(@class,'item-i-9-')]", "Amuleto", "9"),
    MERCENARY("//div[contains(@class,'item-i-15-')]", "Mercenario", "15");

    private final String xpath;
    private final String typeName;
    private final String typeValue;

    Items(String xpath, String typeName, String typeValue){
        this.xpath = xpath;
        this.typeName = typeName;
        this.typeValue = typeValue;
    }

    public String getXpath() {
        return xpath;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public static String getTypeName(String itemClass) {
        Optional<Items> element = Stream.of(Items.values())
        .filter(item -> itemClass.contains(String.format(Constants.ITEMS_TYPE_XPATH_TEMPLATE, item.getTypeValue()))).findFirst();

        return element.map(Enum::toString).orElse(null);
        /*
        for (Items enumItem:Items.values()) {
            if (itemClass.contains(String.format(Constants.ITEMS_TYPE_XPATH_TEMPLATE, enumItem.getTypeValue()))){
                return enumItem.getTypeName();
            }
        }

         */
    }
}

