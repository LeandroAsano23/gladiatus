package constants;

import org.openqa.selenium.By;

public abstract class Constants {

    public static final By bonusMessageBy = By.xpath("//div[@id='header_LoginBonus']");

    public static final By bonusMessageAcceptButtonBy = By.xpath("//input[@value='Recoger bonus']");

    public static final By levelUpMessageBy = By.xpath("//div[@id='header_notification' and contains(text(),'Nivel')]");

    public static final By levelUpMessageAcceptButtonBy = By.xpath("//td[@id='buttonleftnotification']/input");

    public static final By objectiveReachMessageBy = By.xpath("//div[@id='header_notification' and contains(text(),'Objetivo alcanzado')]");

    public static final By objectiveReachAcceptButtonBy = By.xpath("//td[@id='buttonrightnotification']/input");

    public static final By cofferMessageBy = By.xpath("//div[@id='header_notification' and contains(text(),'Enhorabuena')]");

    public static final By cofferNoButtonBy = By.xpath("//td[@id='buttonrightnotification']/input");

    public static final By maintanceMessageBy = By.xpath("//div[@id='title_infobox' and contains(text(),'Mantenimiento')]");

    public static final By maintanceButtonBy = By.xpath("//input[@class='awesome-button big']");

    public static final By itemTypeDropdownBy = By.xpath("//select[@name='itemType']");

    public static final By timeDropdownBy = By.xpath("//select[@id='workTime']");

    public static final By durationDropdownBy = By.xpath("//select[@id='dauer']");

    public static final String INVENTORY_TABS_TEMPLATE = "//div[@id='inventory_nav']//a[contains(@class,'awesome-tabs')][%s]";

    public static final String INVENTORY_ITEM_BOUGHT_TEMPLATE = "//div[@id='inv']//div[contains(@class,'item-i') and contains(@data-tooltip,'%s') and contains(@data-tooltip,'Nivel 1')]";

    public static final String DURATION_2HS = "2 h";

    public static final String DURATION_8HS = "8 h";

    public static final String DURATION_24HS = "24 h";

    public static final String A_WITH_ACCENT_CHAR = "\\u00e1";

    public static final String E_WITH_ACCENT_CHAR = "\\u00e9";

    public static final String I_WITH_ACCENT_CHAR = "\\u00ed";

    public static final String O_WITH_ACCENT_CHAR = "\\u00f3";

    public static final String U_WITH_ACCENT_CHAR = "\\u00fa";

    public static final String Ñ_CHAR = "\\u00f1";

    public static final String ITEMS_TYPE_XPATH_TEMPLATE = "item-i-%s-";

    public static final String FOOD_XPATH_TEMPLATE = "//div[contains(@data-tooltip,'%s')]";

    public static final String[] FOOD_TYPES = {"Banquete","Torta","Manzana","Queso", "Pan","Pescado","Bol","Bife","Pedazo","Sandwich","Pollo", "Bananas", "Carne", "Hamburguesa", "Rollo","Poci"+O_WITH_ACCENT_CHAR+"n","Enrollado de Carne"};

    private Constants(){
    }

}
