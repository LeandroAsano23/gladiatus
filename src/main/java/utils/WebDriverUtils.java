package utils;

import constants.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.time.Duration;
import java.util.Optional;
import java.util.Set;

public abstract class WebDriverUtils {

    private WebDriverUtils(){
    }

    /**
     * @param driver WebDriver
     * @author unknown
     */
    public static void switchToSecondWindow(WebDriver driver) throws Exception {
        String homePageHandler = null;
        homePageHandler = driver.getWindowHandle();
        Set<String> allHandlers = driver.getWindowHandles();
        String handlerOut = homePageHandler;
        for (String handler : allHandlers) {
            if (!handler.equals(homePageHandler)) {
                driver.switchTo().window(handler);
                handlerOut = handler;
                break;
            }
        }
        if (homePageHandler.equals(handlerOut)) {
            throw new Exception("Element not found: unable to switch to second window.");
        }
    }

    public static void switchToSecondWindow(WebDriver driver, String title) throws Exception {
        String homePageHandler = driver.getWindowHandle();  //will keep current window to switch back
        Set<String> allHandlers = driver.getWindowHandles();
        String handlerOut = homePageHandler;
        for(String handler : allHandlers){
            handlerOut = handler;
            if (driver.switchTo().window(handler).getTitle().contains(title)) {
                break;
            }
        }
        if (homePageHandler.equals(handlerOut)) {
            throw new Exception("Element not found: unable to switch to second window.");
        }
    }

    public static void performMouseOver(WebDriver driver, WebElement element) throws InterruptedException {
        WaitUtils.runRandomSleepBetween2And4();
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();
    }
    public static void performDragAndDrop(WebDriver driver, WebElement fromElement, WebElement toElement)  {
        WaitUtils.runRandomSleepBetween2And4();
        WaitUtils.clickeableElement(driver, fromElement, 5);
        WaitUtils.clickeableElement(driver, toElement, 5);

        Actions action = new Actions(driver);
        action.dragAndDrop(fromElement, toElement).build().perform();
    }

    public static void performDragAndDrop(WebDriver driver, Optional<WebElement> fromElement, WebElement toElement) throws InterruptedException {
        WaitUtils.runRandomSleepBetween2And4();

        fromElement.ifPresent(webElement -> {
            WaitUtils.clickeableElement(driver, webElement, 5);
            WaitUtils.clickeableElement(driver, toElement, 5);

            Actions action = new Actions(driver);

            action.dragAndDrop(fromElement.get(), toElement).build().perform();
        });
    }

    public static void performClickAndHoldWithIntermediateElemDrop(WebDriver driver, WebElement fromElement, WebElement intermediateElement, WebElement toElement) throws InterruptedException {
        WaitUtils.runRandomSleepBetween2And4();
        Actions action = new Actions(driver);
        action.clickAndHold(fromElement)
                .pause(Duration.ofSeconds(1))
                .moveToElement(intermediateElement)
                .pause(Duration.ofSeconds(1))
                .release()
                .build()
                .perform();

        WaitUtils.runRandomSleepBetween2And4();

        WaitUtils.clickeableElement(driver, fromElement, 4);
        WaitUtils.clickeableElement(driver, toElement, 4);

        action.dragAndDrop(fromElement, toElement).build().perform();
    }

    public static void selectInventoryTab(WebDriver driver, String tabNumber){
        By tabBy = By.xpath(String.format(Constants.INVENTORY_TABS_TEMPLATE, tabNumber));
        WebElement tab = driver.findElement(tabBy);
        WaitUtils.clickeableElement(driver, tab, 5);
        WaitUtils.clickElement(driver, tab, 5);
    }
}
