package utils;

import com.slack.api.Slack;
import com.slack.api.methods.SlackApiException;

import java.io.IOException;

public class SlackUtils {

    private SlackUtils(){
    }

    public static void sendMessage(String message) throws IOException, SlackApiException {

        GetProperties properties = new GetProperties();

        String slackToken = properties.getProperty("SLACK_TOKEN");
        String slackChannel = properties.getProperty("CHANNEL");

        Slack slack = Slack.getInstance();
        slack.methods(slackToken).chatPostMessage(req ->
                req.channel(slackChannel).asUser(true).text(message));
    }

}
