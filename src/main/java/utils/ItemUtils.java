package utils;

import constants.Colours;
import constants.Icons;
import constants.Items;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebElement;

import java.util.LinkedHashMap;
import java.util.Map;

public final class ItemUtils {

    private ItemUtils(){
    }

    public static Map<String,String> getCharacteristics(WebElement item){
        Map<String, String> characteristics = new LinkedHashMap<>();

        String type = Items.getTypeName(item.getAttribute("class"));
        String itemData = Utils.removeAccents(Utils.replaceUnicodedString(item.getAttribute("data-tooltip"))
                .split("Refinamiento")[0]
                .concat("Tipo: "+type+"."));

        characteristics.put("Nombre",StringUtils.substringBetween(itemData,"[[[\"","\""));
        characteristics.put("Tipo", StringUtils.substringBetween(itemData,"Tipo:","."));
        characteristics.put("Nivel", StringUtils.substringBetween(itemData,"[\"Nivel","\""));
        characteristics.put("Daño", StringUtils.substringBetween(itemData,"[\"Daño","\""));
        characteristics.put("Armadura", StringUtils.substringBetween(itemData,"[\"Armadura","\""));
        characteristics.put("Habilidad", StringUtils.substringBetween(itemData,"[\"Habilidad","\""));
        characteristics.put("Agilidad", StringUtils.substringBetween(itemData,"[\"Agilidad","\""));
        characteristics.put("Constitución", StringUtils.substringBetween(itemData,"[\"Constitución","\""));
        characteristics.put("Carisma", StringUtils.substringBetween(itemData,"[\"Carisma","\""));
        characteristics.put("Valor de ataque critico", StringUtils.substringBetween(itemData,"[\"Valor de ataque","\""));
        characteristics.put("Valor de bloqueo", StringUtils.substringBetween(itemData,"[\"Valor de bloqueo","\""));
        characteristics.put("Valor de endurecimiento", StringUtils.substringBetween(itemData,"[\"Valor de endurecimiento","\""));
        characteristics.put("Amenaza", StringUtils.substringBetween(itemData,"[\"Amenaza","\""));
        characteristics.put("Precio", StringUtils.substringBetween(itemData,"Price: ","."));

        return characteristics;
    }

    public static Map<String,String> getCharacteristicsWithAccents(WebElement item){
        Map<String, String> characteristics = new LinkedHashMap<>();

        String type = Items.getTypeName(item.getAttribute("class"));
        String itemData = Utils.replaceUnicodedString(item.getAttribute("data-tooltip")
                .split("Refinamiento")[0]
                .concat("Tipo: "+type+"."));

        characteristics.put("Nombre",StringUtils.substringBetween(itemData,"[[[\"","\""));
        characteristics.put("Tipo", StringUtils.substringBetween(itemData,"Tipo:","."));
        characteristics.put("Nivel", StringUtils.substringBetween(itemData,"[\"Nivel","\""));
        characteristics.put("Daño", StringUtils.substringBetween(itemData,"[\"Daño","\""));
        characteristics.put("Armadura", StringUtils.substringBetween(itemData,"[\"Armadura","\""));
        characteristics.put("Habilidad", StringUtils.substringBetween(itemData,"[\"Habilidad","\""));
        characteristics.put("Agilidad", StringUtils.substringBetween(itemData,"[\"Agilidad","\""));
        characteristics.put("Constitución", StringUtils.substringBetween(itemData,"[\"Constitución","\""));
        characteristics.put("Carisma", StringUtils.substringBetween(itemData,"[\"Carisma","\""));
        characteristics.put("Valor de ataque critico", StringUtils.substringBetween(itemData,"[\"Valor de ataque","\""));
        characteristics.put("Valor de bloqueo", StringUtils.substringBetween(itemData,"[\"Valor de bloqueo","\""));
        characteristics.put("Valor de endurecimiento", StringUtils.substringBetween(itemData,"[\"Valor de endurecimiento","\""));
        characteristics.put("Amenaza", StringUtils.substringBetween(itemData,"[\"Amenaza","\""));
        characteristics.put("Precio", StringUtils.substringBetween(itemData,"Price: ","."));

        return characteristics;
    }

    public static Map<String,String> getCharacteristicsForMessage(String item){
        Map<String, String> characteristics = new LinkedHashMap<>();
        String itemColour = Colours.getColour(item);
        String itemIcon = Icons.getIcon(item);

        characteristics.put(itemColour,StringUtils.substringBetween(item,"[[[\"","\""));
        characteristics.put("Tipo", itemIcon);
        characteristics.put("Nivel", StringUtils.substringBetween(item,"[\"Nivel","\""));
        characteristics.put("Daño", StringUtils.substringBetween(item,"[\"Daño","\""));
        characteristics.put("Armadura", StringUtils.substringBetween(item,"[\"Armadura","\""));
        characteristics.put("Habilidad", StringUtils.substringBetween(item,"[\"Habilidad","\""));
        characteristics.put("Agilidad", StringUtils.substringBetween(item,"[\"Agilidad","\""));
        characteristics.put("Constitución", StringUtils.substringBetween(item,"[\"Constitución","\""));
        characteristics.put("Carisma", StringUtils.substringBetween(item,"[\"Carisma","\""));
        characteristics.put("Valor de ataque critico", StringUtils.substringBetween(item,"[\"Valor de ataque","\""));
        characteristics.put("Valor de bloqueo", StringUtils.substringBetween(item,"[\"Valor de bloqueo","\""));
        characteristics.put("Valor de endurecimiento", StringUtils.substringBetween(item,"[\"Valor de endurecimiento","\""));
        characteristics.put("Amenaza", StringUtils.substringBetween(item,"[\"Amenaza","\""));
        characteristics.put("Precio", StringUtils.substringBetween(item,"Price: ","."));

        return characteristics;
    }

    public static String formatCharacteristics(Map<String, String> characteristics, String timeRemaining){
        StringBuilder message = new StringBuilder();

        characteristics.forEach((key, value) -> {
            if (value != null) {
                message.append(key);
                message.append(" ");
                message.append(value);
                message.append("\n");

            }
        });
        message.append("\n *Tiempo restante: ");
        message.append(timeRemaining);
        message.append("*\n");
        message.append("----------------------------------------");
        return message.toString();
    }
}
