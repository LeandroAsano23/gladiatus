package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetProperties extends Properties{

    public GetProperties() throws IOException {
        InputStream input = new FileInputStream("src/main/resources/config.properties");
        load(input);
    }

}
