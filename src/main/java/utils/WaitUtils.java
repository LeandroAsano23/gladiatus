package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;

public final class WaitUtils {

    private WaitUtils(){}

    public static void clickeableElement(WebDriver driver, WebElement webElement, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static boolean isClickeableElement(WebDriver driver,WebElement webElement, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
        return true;
    }

    public static void visibilityOfAllElements(WebDriver driver, By locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public static void visibilityOfAllElements(WebDriver driver, List<WebElement> webElements, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOfAllElements(webElements));
    }

    public static void visibilityOfElement(WebDriver driver, WebElement webElement, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void invisibilityToAllElements(WebDriver driver, By locator, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public static void waitForElementDissapear(WebDriver driver, WebElement webElement, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.invisibilityOf(webElement));
    }

    public static boolean isPresent(WebDriver driver, By locator, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
        }catch (TimeoutException e){
            return false;
        }
        return true;
    }

    public static boolean isPresent(WebDriver driver, WebElement webElement, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.visibilityOf(webElement));
        } catch (TimeoutException e) {
            return false;
        }
        return webElement.isDisplayed() && webElement.isEnabled();
    }

    public static boolean isNotPresent(WebDriver driver, By locator, int timeout){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return true;
    }

    public static boolean waitForSecondWindow(WebDriver driver, String windowTitle, int timeOut) throws InterruptedException {
        int time = 0;
        String homePageHandler;
        while (time <= timeOut) {

            try {
                homePageHandler = driver.getWindowHandle();
            } catch (Exception e) {
                return false;
            }

            Set<String> allHandlers = driver.getWindowHandles();

            for (String handler : allHandlers) {
                if (!handler.equals(homePageHandler) && (handler.contains(windowTitle))) {
                        return true;
                }
            }
            time++;
            Thread.sleep(1000);
        }
        return false;
    }

    public static void waitSeconds(int seconds){
        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitMinuts(int minutes){
        try {
            Thread.sleep(minutes*60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void runRandomSleep(int min, int max) throws InterruptedException {
        int seconds = Utils.getRandomSecondsMilisecs(min, max);
        Thread.sleep(seconds);
    }

    public static void runRandomSleepBetween2And4(){
        int seconds = Utils.getRandomSecondsMilisecs(2, 4);
        try {
            Thread.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void clickElement(WebDriver driver,WebElement webElement, int timeout){
        WaitUtils.isPresent(driver, webElement, timeout);
        WaitUtils.clickeableElement(driver, webElement, timeout);
        webElement.click();
    }

    public static void clickElement(WebDriver driver,WebElement webElement){
        WaitUtils.clickeableElement(driver, webElement, 1);
        webElement.click();
    }


    public static void clearAndsendKeys(WebDriver driver, WebElement webElement, String keys, int timeout){
        WaitUtils.clickeableElement(driver, webElement, timeout);
        webElement.clear();
        webElement.sendKeys(keys);
    }

    public static void sendKeys(WebDriver driver, WebElement webElement, String keys, int timeout){
        WaitUtils.clickeableElement(driver, webElement, timeout);
        webElement.sendKeys(keys);
    }

    public static void sendKeys(WebDriver driver, WebElement webElement, String keys){
        WaitUtils.clickeableElement(driver, webElement, 1);
        webElement.sendKeys(keys);
    }

}
