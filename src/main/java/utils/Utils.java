package utils;

import constants.Constants;

import java.util.Arrays;

public final class Utils {

    public static int getRandomNumber(double min, double max){
        double x = (Math.random()*((max-min)+1))+min;
        return (int) x;
    }

    public static int getRandomSecondsMilisecs(double min, double max){
        double x = (Math.random()*((max-min)+1))+min;
        return (int) x*1000;
    }

    public static boolean arrayStrContainsValue(String element, String[] array){
        return Arrays.stream(array).parallel().noneMatch(element::contains);
    }

    public static String replaceUnicodedString(String str){
     return str
             .replace(Constants.Ñ_CHAR,"ñ")
             .replace(Constants.A_WITH_ACCENT_CHAR,"á")
             .replace(Constants.E_WITH_ACCENT_CHAR, "é")
             .replace(Constants.I_WITH_ACCENT_CHAR, "í")
             .replace(Constants.O_WITH_ACCENT_CHAR,"ó")
             .replace(Constants.U_WITH_ACCENT_CHAR, "ú");
    }

    public static String replaceStringToUnicode(String str){
        return str
                .replace("ñ",Constants.Ñ_CHAR)
                .replace("á", Constants.A_WITH_ACCENT_CHAR)
                .replace("é",Constants.E_WITH_ACCENT_CHAR)
                .replace("í",Constants.I_WITH_ACCENT_CHAR)
                .replace("ó",Constants.O_WITH_ACCENT_CHAR)
                .replace("ú",Constants.U_WITH_ACCENT_CHAR);
    }


    public static String removeAccents(String str){
        return str
                .replace("á","a")
                .replace("é","e")
                .replace("í","i")
                .replace("ó","o")
                .replace("ú","u");
    }

}
