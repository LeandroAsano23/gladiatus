import base.BaseGeneral;
import com.slack.api.methods.SlackApiException;
import excepcions.MaintenanceExcepcion;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;
import pages.alliance.Alliance;
import pages.alliance.AllianceMarket;
import pages.circo.BaseCirco;
import pages.circo.Provinciarum;
import pages.circo.ProvinciarumResult;
import pages.topbar.Packages;
import utils.GetProperties;
import utils.ItemUtils;
import utils.SlackUtils;
import utils.WaitUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class RunCombats extends BaseTest {

    @Test
    public void runCombats() throws Exception {
        Lobby lobby = getGladiatusLobby();

        GeneralView general = lobby.clickJugado();

        playWithMaintance(general);
    }

    private void playWithMaintance(BaseGeneral page) throws Exception {
        while (true){
            try {
                playLoop(page);
            } catch (MaintenanceExcepcion e){
                SlackUtils.sendMessage(":warning: Mantenimiento :warning:");
                Lobby lobby = new Lobby(page.getDriver());
                page = lobby.clickJugado();
            }
        }
    }

    /**
     *
     * @param page
     * @throws Exception
     */

    private void playLoop(BaseGeneral page) throws Exception {
        boolean checkauctionhouse = Boolean.parseBoolean(properties.getProperty("CHECKAUCTIONHOUSE_ON"));
        boolean doProvinciarum = Boolean.parseBoolean(properties.getProperty("PROVINCIARUM_ON"));
        boolean rotativesOn = Boolean.parseBoolean(properties.getProperty("ROTATIVES_ON"));
        boolean workOn = Boolean.parseBoolean(properties.getProperty("WORK_ON"));
        boolean foundryOn = Boolean.parseBoolean(properties.getProperty("FOUNDRY_ON"));

        while (true) {
            int auctionTimes = 10;
            int rotativeTimes = 15;
            int foundryTimes = 15;

            while (page.getExpeditionPointsValue()!=0 || page.getDungeonPointsValue()!=0){
                int waitSeconds = 60;

                //CHECK AUCTION HOUSE
                if (checkauctionhouse && auctionTimes==10){
                    checkAuctionHouse(page);
                    auctionTimes=0;
                }

                //EXPEDITIONS
                if (page.getExpeditionPointsValue()>0){
                    page = doExpedition(page);
                    waitSeconds-=20;
                }

                //DUNGEON
                if (page.getDungeonPointsValue()>0){
                    page = doDungeon(page);
                    waitSeconds-=20;
                }

                //PROVINCIARUM
                if (doProvinciarum){
                    page = doProvinciarum(page);
                }

                //Rotatives
                if (rotativesOn && rotativeTimes==15){
                    buyAndSellRotative(page);
                    rotativeTimes=0;
                }

                //FOUNDRY
                if (foundryOn && foundryTimes==15){
                    doFoundryTasks(page);
                    foundryTimes = 0;
                }

                //WAIT AND RE-INICIALIZE
                WaitUtils.waitSeconds(waitSeconds);
                foundryTimes++;
                rotativeTimes++;
                auctionTimes++;
                System.out.println("rotatives: "+rotativeTimes+" - foundryTimes:"+foundryTimes);
            }
            //WORK
            if (workOn){
                doWork(page);
            }
        }
    }

    private void doFoundryTasks(BaseGeneral page) throws InterruptedException, IOException, SlackApiException, MaintenanceExcepcion {
        String tab = properties.getProperty("MELT_ITEMS_TAB");

        Packages packages = page.goPackages();
        packages.fullInventory(tab);

        Foundry foundry = page.goFoundry();
        foundry.getMeltedItems(page);
        foundry.meltItem(page, tab);
    }

    private void doWork(BaseGeneral page) throws InterruptedException, IOException, SlackApiException, MaintenanceExcepcion {
        String workTitle = properties.getProperty("WORK_TITLE");
        String workHours = properties.getProperty("WORK_HOURS").trim();

        Work work = page.goWork();
        work.selectJob(workTitle);
        int workMins = work.selectTime(workHours);
        work.startWork();
        SlackUtils.sendMessage(":construction_worker: Trabajando por "+workMins+" mins");

        WaitUtils.waitMinuts(workMins);
        work.goGeneralView();
    }

    private void buyAndSellRotative(BaseGeneral page) throws Exception {
        Alliance alliance = page.goAlliance();
        AllianceMarket allianceMarket = alliance.goMarket();
        String itemBoughtName = allianceMarket.buyItem();

        if (itemBoughtName!=null){
            Packages packages = page.goPackages();
            packages.selectTab("1");
            packages.dragItemToInventory();

            alliance = packages.goAlliance();
            allianceMarket = alliance.goMarket();
            allianceMarket.sellItem(itemBoughtName);
        }
    }

    private ExpeditionResult doExpedition(BaseGeneral page) throws Exception {
        String monster = properties.getProperty("MONSTER");

        checkHealth(page);

        Expeditions expeditions = page.goExpedition();
        WaitUtils.runRandomSleepBetween2And4();

        ExpeditionResult expeditionResult = expeditions.attackMonster(monster);
        WaitUtils.runRandomSleepBetween2And4();

        expeditionResult.checkPopUps();

        return expeditionResult;
    }

    private DungeonResult doDungeon(BaseGeneral page) throws Exception {
        boolean advanced = Boolean.parseBoolean(properties.getProperty("ADVANCED"));

        Dungeon dungeon = page.goDungeon();
        WaitUtils.runRandomSleepBetween2And4();

        dungeon = dungeon.enterDungeon(advanced);
        Assert.assertNotNull(dungeon, "Verify \"Advanced\" button not disabled");

        DungeonResult dungeonResult = dungeon.attackDungeon();
        WaitUtils.runRandomSleepBetween2And4();

        dungeon.checkPopUps();

        return dungeonResult;
    }

    private ProvinciarumResult doProvinciarum(BaseGeneral page) throws MaintenanceExcepcion {
        BaseCirco circo = page.goCirco();
        WaitUtils.runRandomSleepBetween2And4();

        Provinciarum provinciarum = circo.goProvinciarum();
        WaitUtils.runRandomSleepBetween2And4();

        ProvinciarumResult provinciarumResult = provinciarum.attackRandom();

        provinciarumResult.checkPopUps();

        return provinciarumResult;
    }

    /**
     *
     * @param page
     * @throws InterruptedException
     */

    private void checkHealth(BaseGeneral page) throws InterruptedException, IOException, SlackApiException, MaintenanceExcepcion {
        int minimunLife = Integer.parseInt(properties.getProperty("MINIMUN_LIFE"));
        String foodTab = properties.getProperty("FOOD_TAB");

        if (!page.isHealthBarSufficient(minimunLife)){
            GeneralView generalView = page.goGeneralView();
            generalView.selectTab(foodTab);
            generalView.eatSomething();
        }
    }

    private void checkAuctionHouse(BaseGeneral page) throws InterruptedException, MaintenanceExcepcion {
        String tab = properties.getProperty("AUCTIONTAB");

        AuctionHouse auctionHouse = page.goAuctionHouse();

        //Check Both Gladiators and Mercenaries
        if (tab.equals("All")){
            checkTab(auctionHouse.clickGladiatorsTab(), properties);
            checkTab(auctionHouse.clickMercenariesTab(), properties);

            //Gladiators of Mercenaries
        } else {
            auctionHouse = (tab.equals("Mercenaries"))
                    ? auctionHouse.clickMercenariesTab()
                    : auctionHouse.clickGladiatorsTab();
            checkTab(auctionHouse, properties);
        }
    }

    private static void checkTab(AuctionHouse auctionHouse, GetProperties properties) throws MaintenanceExcepcion {
        String[] mods = properties.getProperty("OBJECT_MOD").split(",");

        auctionHouse.selectObjectType("Todo");
        auctionHouse = auctionHouse.clickFilter();

        AuctionHouse finalAuctionHouse = auctionHouse;
        Arrays.stream(mods).forEach(mod -> {

            List<String> matchItems = finalAuctionHouse.getItems(mod);

            matchItems.forEach(matchedItem -> {
                Map<String, String> characteristics = ItemUtils.getCharacteristicsForMessage(matchedItem);

                String message = ItemUtils.formatCharacteristics(characteristics, finalAuctionHouse.getTimeRemaining());

                try {
                    SlackUtils.sendMessage(message);
                } catch (IOException | SlackApiException e) {
                    e.printStackTrace();
                }
            });
        });

    }

    /**
     * Can enter lobby through facebook or with a gameforge account
     * @param email
     * @param password
     * @param login
     * @return
     * @throws Exception
     */

    private Lobby enterLobby(String email, String password, Login login) throws Exception {
        boolean facebook = Boolean.parseBoolean(properties.getProperty("FACEBOOK"));
        if (!facebook){
            login.loadEmail(email);
            login.loadPassword(password);
            return login.clickLogin();
        } else {
            FacebookLogin facebookLogin = login.clickLoginThroughFacebook();
            facebookLogin.loadEmail(email);
            facebookLogin.loadPassword(password);
            return facebookLogin.clickLogin();
        }
    }

}
