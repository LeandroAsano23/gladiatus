import base.Driver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import pages.FacebookLogin;
import pages.Lobby;
import pages.Login;
import utils.GetProperties;
import utils.WaitUtils;

public abstract class BaseTest {

    protected GetProperties properties;

    private Lobby gladiatusLobby;

    @BeforeClass(alwaysRun = true)
    @Parameters({"email","password","browser"})
    public void beforeSuite(String email, String password, String browser) throws Exception {
        properties = new GetProperties();
        Driver driver = new Driver(browser);
        Login login = new Login(driver.getWebdriver());

        login.clickIniciarTab();
        WaitUtils.waitSeconds(2);
        gladiatusLobby = enterLobby(email, password, login, properties);
    }

    /**
     * Can enter lobby through facebook or with a gameforge account
     * @param email
     * @param password
     * @param login
     * @param properties
     * @return
     * @throws Exception
     */

    private Lobby enterLobby(String email, String password, Login login, GetProperties properties) throws Exception {
        boolean facebook = Boolean.parseBoolean(properties.getProperty("FACEBOOK"));
        if (!facebook){
            login.loadEmail(email);
            login.loadPassword(password);
            return login.clickLogin();
        } else {
            FacebookLogin facebookLogin = login.clickLoginThroughFacebook();
            facebookLogin.loadEmail(email);
            facebookLogin.loadPassword(password);
            return facebookLogin.clickLogin();
        }
    }

    @AfterClass
    public void afterSuite() {
        //getGladiatusLogin().dispose();
    }

    public Lobby getGladiatusLobby() {
        return gladiatusLobby;
    }

    public GetProperties getProperties() {
        return properties;
    }

}
